import i2cdriver

i2c = i2cdriver.I2CDriver("/dev/ttyUSB0")

DEVICE      = 0x42
SET_POINTER = 0x0C
READ_VALUE  = 0x0D

def set_ptr(v):
    # 0 is write, 1 is read
    i2c.start(DEVICE, 0)
    i2c.write([SET_POINTER, v])
    i2c.stop()

def read_mem():
    i2c.start(DEVICE, 0)
    i2c.write([READ_VALUE])
    i2c.stop()
    i2c.start(DEVICE, 1)
    result = i2c.read(2)
    i2c.stop()
    return result

for rep in range(0, 10):
    print("REP: {0}".format(rep))
    for i in range(0,3):
        set_ptr(i)
        result = read_mem()
        s = ", ".join(hex(ord(c)) for c in result)
        print(s)
