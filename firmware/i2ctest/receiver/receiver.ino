// MCJ 20190511
// Resources
// http://dsscircuits.com/articles/arduino-i2c-slave-guide
// http://nilhcem.com/android-things/arduino-as-an-i2c-slave
// https://www.arduino.cc/en/Tutorial/MasterWriter

#include <Wire.h>

#define  MY_ADDRESS           0x42  // Any number from 0x01 to 0x7F

// uint8_t vs byte vs unsigned char
// https://oscarliang.com/arduino-difference-byte-uint8-t-unsigned-cha/
byte opcode;
byte pointer = 0;
byte secrets[] = {8, 42, 137};

#define FALSE 0
#define TRUE  1
byte msg = FALSE;

#define WOQI 12
#define QOWI 11
#define SS   10
#define CLK  13
#define LED  WOQI


// // // // // // // // //
// SETUP
void setup() {
  Wire.begin(MY_ADDRESS);
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);
  pinMode(LED, OUTPUT);
}

// // // // // // // // //
// LOOP
void loop() {

}


void slowToggle(int ms) {
  digitalWrite(LED, HIGH);
  delay(ms);
  digitalWrite(LED, LOW);
  delay(ms);
}

void blinkN(int n, int ms) {
  slowToggle(200);
  slowToggle(200);
  delay(4 * ms);
  for (int i = 0 ; i < n ; i++) {
    digitalWrite(LED, HIGH);
    delay(ms);
    digitalWrite(LED, LOW);
    delay(ms);
  }
  delay(4 * ms);
}



enum {
  SET_POINTER = 0x0C,
  READ_VALUE  = 0x0D
};


// This will be called when data is requested from us.
void requestEvent() {
  // Queen setting the register.
  switch (opcode) {
    case READ_VALUE:
      byte m[3];
      m[0] = pointer;
      m[1] = secrets[pointer];
      m[2] = 0xEF;
      Wire.write(m, 2);
      // msg = TRUE;
      break;
    default:

      Wire.write(0xEE);
      break;
  }
}

// This is called when the queen sends data to us.
// The register should always be set first.
void receiveEvent(int num_bytes_received) {
  msg = num_bytes_received;

  opcode = Wire.read();
  // Queen setting the register.
  switch (opcode) {
    case SET_POINTER:
      pointer = (byte)Wire.read();
      // msg = TRUE;
      break;
    case READ_VALUE:
      break;
  }
}
