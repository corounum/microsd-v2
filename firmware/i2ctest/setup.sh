virtualenv venv
source venv/bin/activate

pip install -r requirements.txt

# Make sure things run fast.
setserial /dev/ttyUSB0 low_latency