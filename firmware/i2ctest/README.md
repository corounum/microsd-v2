# Testing the I2C Protocol

At the momnt, the code in 

receiver/receiver.ino

Is a test file. It implements a simple protocol that lets us read values from the uSD Layer (USDL). It needs to be uploaded onto the USDL from the Arduino environment before anything will work. 

The test.py file also critical. However, to use it, you first have to set up the Python environment. This is done with a virtual environment.

What that means is that we run the "virtualenv" command, which creates a folder (called "venv"). This is a place where a copy of Python is "installed." Then, whenever we run Python, it runs from this folder.

That is useful, because when we install things (with "pip"), it installs to that folder. It doesn't require special permissions (eg. "root".)

So, the way you should get started is:

$ source setup.sh
$ python test.py

if you've installed everything on the Arduino, and have the I2CDriver set up correctly.

It should output something like:

REP: 0
0x0, 0x8
0x1, 0x2a
0x2, 0x89
REP: 1
0x0, 0x8
0x1, 0x2a
0x2, 0x89
REP: 2
0x0, 0x8
0x1, 0x2a
0x2, 0x89
REP: 3
0x0, 0x8
0x1, 0x2a
0xee, 0xff
REP: 4
0x0, 0x8
0x1, 0x2a
0x2, 0x89
REP: 5
0xff, 0xff
0x1, 0x2a
0x2, 0x89
REP: 6
0x0, 0x8
0x1, 0x2a
0x2, 0x89
REP: 7
0x0, 0x8
0x1, 0x2a
0x2, 0x89
REP: 8
0x0, 0x8
0x1, 0x2a
0x2, 0x89
REP: 9
0x0, 0x8
0x1, 0x2a
0x2, 0x89

which is 10 repetitions of reading from the memory of the ATMega328p on the board.

# EMAIL NOTES
A history of notes about this code. Saves writing other docs. Not well organized, but it's archived.

## 2019-05-12
Hi all,

I wrote an I2C "protocol test".

https://bitbucket.org/corounum/microsd-v2/src/master/firmware/i2ctest/receiver/receiver.ino

It listens for commands from an I2C device. In particular, you can:

1. set the address in memory that you want to read from (possible values of 0, 1, or 2), and
2. you can read a value at the current address.

This models how many of the devices we've been using work: we send an I2C message to set an address in memory, then we do a read.

I then wrote a script to test this:

https://bitbucket.org/corounum/microsd-v2/src/master/firmware/i2ctest/test.py

We have been using the I2C driver as a command-line program, but you can also script it with Python. I got tired of typing commands one-at-a-time (and, I think I was doing it wrong), so this allowed me to reliably/repeatably test the layer. (Think of this as the first step towards having a hardware test rig, so we know if a layer is working.)

I put a copy of this in the README in that folder. It's enough to serve as a reminder of how to do things.

I also set up a Linux machine, and it will live in the lab as a permanent workstation for doing work with the boards. We're soon going to (or have already) reached the point where having an environment that "just works" over-and-over, every time, for programming the boards matters.

It will likely be Monday before I can solder a jumper to test the SRAM. Once that is in place, we can expand the I2C protocol, and begin integrating our I2C protocol with the OpenLog code.

Perhaps easier said than done, but it looks doable, from reading the OpenLog code. This experiment goes a long way to convincing me that it is possible to do this.

Also, I have come to realize that our boards often have no extra pins connected. As a result, it's really hard to debug a layer. When we fix this layer, we should think about connectnig (say) pin0 and pin1 on the header, so that we always have somewhere to plug in some LEDs for blinking. (Or, we should think about  including an LED, or a Neopixel, so that it is possible to debug these things...)

Cheers,
M

## RESOURCES
https://arduino.stackexchange.com/questions/22898/how-to-design-and-debug-a-custom-i2c-master-slave-system

http://www.gammon.com.au/i2c

https://github.com/madsci1016/Arduino-EasyTransfer

https://www.i2c-bus.org/fileadmin/ftp/i2c_bus_specification_1995.pdf

https://dronebotworkshop.com/i2c-arduino-arduino/

http://nilhcem.com/android-things/arduino-as-an-i2c-slave

https://i2cdriver.com/i2cdriver.pdf

https://ns-electric.com/knowledge-base/intro-to-arduino-i2c-serial-communication/

